require "rake/testtask"
Rake::TestTask.new(:unit) do |t|
  t.libs.delete 'lib'
  t.test_files = FileList["spec/**/*_spec.rb"]
  t.verbose = true
end


require "cucumber"
require "cucumber/rake/task"
Cucumber::Rake::Task.new(:features) do |t|
  t.libs.delete 'lib'
  t.cucumber_opts = %w{
    features
    -x
    --format progress
    --tags ~@ignore
  }
end

task :default => [:unit, :features]
